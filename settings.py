#!/usr/bin/python3


#Make sure to put modules in the module folder before enabling them
ENABLED_MODULES=['ram', 'disk', 'cpu', 'network']

#Information refresh interval
TIMEOUT = 5

#Won't send data to graphite, but rather just output it to the console
DEBUG = False

###
### CARBON SETTINGS
###

CARBON_SERVER = '0.0.0.0'
CARBON_PORT = 2003
