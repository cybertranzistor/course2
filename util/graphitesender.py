#/usr/bin/python3

import socket
import time
import settings

class GraphiteSender:

	def __init__(self):
		if not settings.DEBUG:
			self.sock = socket.socket()
			self.sock.connect((settings.CARBON_SERVER, settings.CARBON_PORT))

	def send(self,name, data):
		message = '{} {} {}\n'.format(name,data,int(time.time()))
		if settings.DEBUG:
			print(message)
		else:
			self.sock.sendall(message.encode())


