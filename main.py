#!/usr/bin/python3
import settings
import os
import time
from util.graphitesender import GraphiteSender

working = True
tmp = dict()
sender = GraphiteSender()
f = sender.send
while working:
	for module in settings.ENABLED_MODULES:
		mo = __import__("modules."+module)
		exec("mo."+module+".run(tmp,f)")
	time.sleep(settings.TIMEOUT)
