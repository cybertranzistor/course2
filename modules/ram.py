#!/usr/bin/python3
import re
import sys
import time
import settings

def run(tmp,send):
	f = open('/proc/meminfo','r')
	r = f.read().replace('\n',' ').split(' ')
	f.close()
	total = 0
	free = 0
	last = tmp.get('ram',0)
	for x in r:
		if re.match(r'[0-9]+',x):
			if total == 0:
				total = int(x)
			else:
				free = int(x)
				break
	used = total - free
	if last != 0:
		send('ram',used)
	tmp['ram']=used
		
