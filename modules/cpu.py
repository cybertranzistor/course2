#!/usr/bin/python3
import re
import sys
import time
import settings

def run(tmp,send):
	f = open('/proc/stat','r')
	r = f.read().replace('\n',' ').split(' ')
	f.close()
	count = -1
	cpu = ''
	for x in r:
		if re.match(r'.?cpu[0-9]{0,3}',x):
			count = 0
			cpu = x
		elif re.match(r'[0-9]+',x) and 0 <= count <= 9:
			count += 1
			last = tmp.get(cpu+str(count),0)
			if count == 1:
				if last != 0:
					send('cpu.'+cpu+'.user',int(x)-int(last))
				tmp[cpu+str(count)] = x
			if count == 2:
				if last != 0:
					send('cpu.'+cpu+'.nice',int(x)-int(last))
				tmp[cpu+str(count)] = x
			if count == 3:
				if last != 0:
					send('cpu.'+cpu+'.system',int(x)-int(last))
				tmp[cpu+str(count)] = x
		
