#!/usr/bin/python3
import re
import sys
import time
import settings

def run(tmp,send):
	f = open('/proc/diskstats','r')
	r = f.read().replace('\n','').split(' ')
	f.close()
	count = -1
	disk = ''
	for x in r:
		if re.match(r'sd[a-z][0-9]?',x):
			count = 0
			disk = x
		elif re.match(r'[0-9]+',x) and 0 <= count <= 10:
			count += 1
			last = tmp.get(disk+str(count),0)
			if count == 3:
				if last != 0:
					send('disk.'+disk+'.read',int(x)-int(last))
				tmp[disk+str(count)] = x
			if count == 7:
				if last != 0:
					send('disk.'+disk+'.write',int(x)-int(last))
				tmp[disk+str(count)] = x
		
