#!/usr/bin/python3
import re
import sys
import time
import settings


def run(tmp, send):
	f = open('/proc/net/dev', 'r')
	r = f.read().replace("\n","")
	r = r.split(' ')
	f.close()
	count = -1
	interface = ''
	for x in r :
		if re.match(r'(eth|wlan)[0-9]?',x) :
			interface = x[:-1]
			count = 0
		elif re.match(r'[0-9]+',x) and 0 <= count <= 15:
			count += 1
			last = tmp.get(interface+str(count),0)
			if count == 1:
				if last != 0:
					send('net.'+interface+'.receive',int(x)-int(last))
				tmp[interface+str(count)] = x
			if count == 9:
				if last != 0:
					send('net.'+interface+'.transmit',int(x)-int(last))
				tmp[interface+str(count)] = x
		
